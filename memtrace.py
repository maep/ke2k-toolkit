#!/usr/bin/python2
# small script to trace allocations
# edit stuff.h to enable resource tracing

import sys
memmap = {}

for line in sys.stdin:
    tok = line.split()
    if tok[0] == '+':
        addr = int(tok[1], 16)
        memmap[addr] = tok[2]
    elif tok[0] == '-':
        addr = int(tok[1], 16)
        if addr in memmap:
            del memmap[addr]
        else:
            print 'bad address 0x%x' % addr

for k, v in memmap.iteritems():
    print 'leak near %s 0x%x' % (v,k)
