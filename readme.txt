A small SDL2 based widget toolkit written in C99.

Disclaimer:
It's not perfect and it's not really meant to be used. I just wanted to play with SDL2, and half of it was written when I was sick in bed.

Features:
There are just a few basic widgets. Many common widgets have duplicate functionality which is why I didn't implement any of those. For example check box or radio buttons are just a different looking toggle buttons. I've included a file browser dialog window, which was a test to see how well things work together. I also made use of code from my other projects, like a string lib and a few file system functions. The widget core itself is below 1000 lines of code and should be easy to read into. I tried to keep to the KISS principle; form follows function and simplicity trumps speed in most cases. If you find unreasonably complex code or have questions about the code feel free to contact me. It's meant to be a learning experience for both me and the reader. :D

Building:
Premake4 is used to generate gmake, Visual Studio and XCode project files. I works fine with recent gcc and clang. I tried building it with VS2013 Express but it seems the new C99 compiler is not available yet. Maybe I missed some option in the project settings. Once I get it to build I'll include the VS project files. I can't test on XCode, SDL2 doesn't support OSX 10.4. I'm not upgrading, they can pry my PowerPC from my cold dead fingers!

Background:
My original plan was to use this to build a wav editor but so far little materialized. This is actually my third GUI toolkit I wrote. The first one was written in C++ with Boost which I regret in hindsight. It did everything the C++ way, like using make_shared instead of new/delete, with a proper class hierarchy, etcetera. It was about 4 times as much code and could do way less. Maybe it was a tad faster, and certainly more idiot proof but where is the fun in that? The second one was a C port of the first one but it was even worse because I was using C the C++ way. Bad idea. So I threw everything away (except the lessons learned) and started anew.

Third party stuff:
Comfortaa
SIL Open Font License, Version 1.1.
Johan Aakerlund aajohan@gmail.com

CityHash
MIT License
Geoff Pike and Jyrki Alakuijala

Ackowledgements:
Premake4 - makes CMake look bad
Mousepad - my main source editor
Globular - thanks for the free music
