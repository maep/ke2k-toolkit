#include <stdlib.h>
#include <stdio.h>
#include <stdarg.h>
#include <string.h>
#include <strings.h>
#include <ctype.h>
#include <assert.h>
#include "str.h"

static bool str_terminated(struct string s)
{
    return !str_buf(s)[str_len(s)];
}

static uintptr_t str_siz(int len, int cap)
{
    return ((uintptr_t)cap << STR_SHIFT) | ((uintptr_t)len & STR_MASK);
}

static struct string str_wrap_impl(const char* s, int len, long cap)
{
    if (!s)
        return STR("");
    if (len == STR_END)
        len = strlen(s);
    if (cap == STR_END)
        cap = len;
    assert(len >= 0 && len < STR_END && cap >= 0);
    return (struct string) {(uintptr_t) s, str_siz(len, cap)};
}

// grows s to hold at least len characters + null terminator
// sets length to len, zeros bytes after len
static struct string str_grow_and_zero(struct string s, int len)
{
    assert(str_cap(s));
    int capacity = str_cap(s);
    if (len + 1 > capacity) {
        capacity = (len + 16) & ~0xf;
        assert(capacity < STR_END);
        TRACE_FREE(s.str);
        s.str = (uintptr_t)realloc(str_buf(s), capacity);
        TRACE_ALOC(s.str);
    }
    s.siz = str_siz(len, capacity);
    memset(str_buf(s) + len, 0, capacity - len);
    return s;
}

//------------------------------------------

struct string str_wrap_n(const char* s, int len)
{
    return str_wrap_impl(s, len, 0);
}

struct string str_wrap(const char* s)
{
    return str_wrap_impl(s, STR_END, 0);
}

struct string str_own(char* s)
{
    TRACE_ALOC(s);
    return str_wrap_impl(s, STR_END, STR_END);
}

struct string str_new_empty(int capacity)
{
    // add extra byte for terminator and align to next 16 bytes
    capacity = (capacity + 16) & ~0xf;
    assert(capacity < STR_END);
    struct string s = {(uintptr_t)calloc(1, capacity), str_siz(0, capacity)};
    TRACE_ALOC(s.str);
    return s;
}

struct string str_new_format(const char* format, ...)
{
    va_list ap;
    va_start(ap, format);
    int len = vsnprintf(NULL, 0, format, ap);
    va_end(ap);
    struct string s = str_new_empty(len);
    va_start(ap, format);
    vsnprintf(str_buf(s), len + 1, format, ap);
    va_end(ap);
    s.siz = str_siz(len, str_cap(s));
    return s;
}

struct string str_new(const char* s)
{
    return str_duplicate(str_wrap(s));
}

struct string str_duplicate(struct string s)
{
    struct string dst = str_new_empty(str_len(s));
    memmove(str_buf(dst), str_buf(s), str_len(s));
    dst.siz = str_siz(str_len(s), str_cap(dst));
    return dst;
}

void str_free(struct string s)
{
    if (str_cap(s)) {
        free(str_buf(s));
        TRACE_FREE(s.str);
    }
}

//------------------------------------------

const char* str_cstr(struct string s)
{
    STATIC_THREAD_LOCAL char* tmpbuf = NULL;
    STATIC_THREAD_LOCAL int tmpsize = 0;
    if (!str_len(s))
        return "";
    if (str_terminated(s))
        return str_buf(s);
    if (tmpsize < str_len(s) + 1) {
        tmpsize = (str_len(s) + 16) & ~0xf;
        TRACE_FREE(tmpbuf);
        tmpbuf = realloc(tmpbuf, tmpsize);
        TRACE_ALOC(tmpbuf);
    }
    memmove(tmpbuf, str_buf(s), str_len(s));
    tmpbuf[str_len(s)] = 0;
    return tmpbuf;
}

struct string str_substring(struct string s, int begin, int length)
{
    char* str = str_buf(s) + iclamp(0, begin, str_len(s));
    int len = imin(length, str_len(s) - begin);
    return str_wrap_n(str, len);
}

//------------------------------------------

struct string str_format(struct string s, const char* format, ...)
{
    va_list ap;
    va_start(ap, format);
    int len = vsnprintf(NULL, 0, format, ap);
    va_end(ap);
    s = str_grow_and_zero(s, len);
    va_start(ap, format);
    vsnprintf(str_buf(s), len + 1, format, ap);
    va_end(ap);
    return s;
}

struct string str_insert(struct string s, struct string string, int pos)
{
    int inslen = str_len(string);
    int len = str_len(s);
    pos = pos > 0 ? imin(len, pos) : imax(len, len + pos);
    s = str_grow_and_zero(s, len + inslen);
    char* buf = str_buf(s);
    memmove(buf + pos + inslen, buf + pos, len - pos);
    memmove(buf + pos, str_buf(string), inslen);
    return s;
}

struct string str_append(struct string s, struct string string)
{
    return str_insert(s, string, STR_END);
}

struct string str_erase(struct string s, int pos, int len)
{
    assert(str_cap(s));
    int oldlen = str_len(s);
    pos = pos > 0 ? imin(oldlen, pos) : iclamp(0, oldlen + pos, oldlen);
    len = len > 0 ? imin(oldlen - pos, len) : iclamp(0, oldlen + len, oldlen - pos);
    char* buf = str_buf(s);
    memmove(buf + pos, buf + pos + len, oldlen - pos - len);
    s.siz = str_siz(oldlen - len, str_cap(s));
    buf[str_len(s)] = 0;
    return s;
}

struct string str_set(struct string s, struct string string, int pos)
{
    assert(str_cap(s));
    int setlen = str_len(string);
    int len = str_len(s);
    pos = pos > 0 ? imin(len, pos) : imax(len, len + pos);
    s = str_grow_and_zero(s, imax(len, pos + setlen));
    memmove(str_buf(s) + pos, str_buf(string), setlen);
    return s;
}

//------------------------------------------

int str_find_first(struct string haystack, struct string needle)
{
    int lh = str_len(haystack), ln = str_len(needle);
    const char* sh = str_buf(haystack), *sn = str_buf(needle);
    if (lh < ln)
        return STR_END;
    for (int ih = 0, in = 0; ih < lh; ih++) {
        in = (sh[ih] == sn[in]) ? in + 1 : 0;
        if (in == ln)
            return ih - ln;
    }
    return STR_END;
}

int str_find_first_i(struct string haystack, struct string needle)
{
    int lh = str_len(haystack), ln = str_len(needle);
    const char* sh = str_buf(haystack), *sn = str_buf(needle);
    if (lh < ln)
        return STR_END;
    for (int ih = 0, in = 0; ih < lh; ih++) {
        in = (tolower(sh[ih]) == tolower(sn[in])) ? in + 1 : 0;
        if (in == ln)
            return ih - ln;
    }
    return STR_END;
}

int str_find_last(struct string haystack, struct string needle)
{
    int lh = str_len(haystack), ln = str_len(needle);
    const char* sh = str_buf(haystack), *sn = str_buf(needle);
    if (lh < ln)
        return STR_END;
    for (int ih = lh - 1, in = ln - 1; ih >= 0; ih--) {
        in = (sh[ih] == sn[in]) ? in - 1 : ln - 1;
        if (in == -1)
            return ih;
    }
    return STR_END;
}

int str_find_last_i(struct string haystack, struct string needle)
{
    int lh = str_len(haystack), ln = str_len(needle);
    const char* sh = str_buf(haystack), *sn = str_buf(needle);
    if (lh < ln)
        return STR_END;
    for (int ih = lh - 1, in = ln - 1; ih >= 0; ih--) {
        in = (tolower(sh[ih]) == tolower(sn[in])) ? in - 1 : ln - 1;
        if (in == -1)
            return ih;
    }
    return STR_END;
}

bool str_contains(struct string haystack, struct string needle)
{
    return str_find_first(haystack, needle) != STR_END;
}

bool str_contains_i(struct string haystack, struct string needle)
{
    return str_find_first_i(haystack, needle) != STR_END;
}

//------------------------------------------

bool str_starts_with(struct string s, struct string prefix)
{
    int ls = str_len(s), lp = str_len(prefix);
    return lp <= ls && !strncmp(str_buf(s), str_buf(prefix), lp);
}

bool str_starts_with_i(struct string s, struct string prefix)
{
    int ls = str_len(s), lp = str_len(prefix);
    return lp <= ls && !strncasecmp(str_buf(s), str_buf(prefix), lp);
}

bool str_ends_with(struct string s, struct string suffix)
{
    int ls = str_len(s), lp = str_len(suffix);
    return lp <= ls && !strncmp((char*)s.str + ls - lp, (char*)suffix.str, lp);
}

bool str_ends_with_i(struct string s, struct string suffix)
{
    int ls = str_len(s), lp = str_len(suffix);
    return lp <= ls && !strncasecmp(str_buf(s) + ls - lp, str_buf(suffix), lp);
}

//------------------------------------------

int str_compare_i(struct string a, struct string b)
{
    int la = str_len(a), lb = str_len(b);
    int res = strncasecmp(str_buf(a), str_buf(b), imin(la, lb));
    return res ? res : la - lb;
}

bool str_equals_i(struct string a, struct string b)
{
    return str_len(a) == str_len(b) && !str_compare_i(a, b);
}

int str_compare(struct string a, struct string b)
{
    int la = str_len(a), lb = str_len(b);
    int res = strncmp(str_buf(a), str_buf(b), imin(la, lb));
    return res ? res : la - lb;
}

bool str_equals(struct string a, struct string b)
{
    return str_len(a) == str_len(b) && !str_compare(a, b);
}

//------------------------------------------

struct string str_to_lower(struct string s)
{
    assert(str_cap(s));
    char* str = str_buf(s);
    for (int i = 0; i < str_len(s); i++)
        str[i] = tolower(str[i]);
    return s;
}

uint32_t str_hash(struct string s)
{
    return city_hash32(str_buf(s), str_len(s));
}
