#include <assert.h>
#include "widget.h"
#include "stuff.h"

enum {
    FILEBROWSER_NOTIFY = 0x6e41
};

struct filebrowser {
    struct widget*      window;
    struct widget*      name_edit;
    struct widget*      file_list;
    struct widget*      open_button;
    struct widget*      cancel_button;
    struct string       dir;
    struct string_list  list;
    int                 type;
    bool                show_hidden;
    uint32_t            timestamp;
    SDL_Thread*         thread; // remove once SDL_DetachThread is available
    void*               payload;
    void                (*callback) (struct string, void*);
};

static void fb_free(struct filebrowser* fb)
{
    widget_free_all(fb->window);
    str_free(fb->dir);
    free(fb);
    TRACE_FREE(fb);
}

static void fb_scan(struct string_list* list, struct string path, bool show_hidden, bool dirs_only)
{
    // TODO fix scan
    struct string_list dir_list = {0};
    struct string_list file_list = {0};
    SDLX_Dirent ent;
    for (int err = SDLX_FirstDir(&ent, str_cstr(path)); !err; err = SDLX_NextDir(&ent)) {
        if ((!show_hidden && ent.flags & SDLX_FILESYSTEM_HIDDEN) || !strcmp(ent.name, "."))
            continue;
        if (!dirs_only && ent.type == SDLX_FILESYSTEM_FILE)
            string_list_append(&file_list, str_new(ent.name));
        else if (ent.type == SDLX_FILESYSTEM_DIRECTORY)
            string_list_append(&dir_list, str_new_format("%s/", ent.name));
    }
    SDLX_CloseDir(&ent);
    string_list_sort(&dir_list);
    string_list_sort(&file_list);
    for (int i = 0; i < dir_list.size; i++)
        string_list_append(list, dir_list.mem[i]);
    for (int i = 0; i < file_list.size; i++)
        string_list_append(list, file_list.mem[i]);
    string_list_free(&dir_list);
    string_list_free(&file_list);
}

static int fb_scan_thread(void* data)
{
    struct filebrowser* fb = data;
    memset(&fb->list, 0, sizeof fb->list);
    fb_scan(&fb->list, fb->dir, fb->show_hidden, fb->type == FILEBROWSER_OPENDIR);
    // tell widget new data is ready (on main thread)
    // the sdl api says event.type must be created with SDL_RegisterEvents()
    // which just returns SDL_USEREVENT + x
    SDL_Event event;
    SDL_zero(event);
    event.type = SDL_USEREVENT;
    event.user.code = FILEBROWSER_NOTIFY;
    SDL_PushEvent(&event);
    return 0;
}

static void fb_start_scan(struct filebrowser* fb)
{
    widget_set_flags(fb->window, WIDGET_ARMED, 0); // arm widget so it will recieve SDL_USEREVENT
    fb->thread = SDL_CreateThread(fb_scan_thread, "dir_scan", fb);
    // SDL_DetachThread(thread); // added in SDL 2.0.2
}

static struct string fb_change_path(struct string path, struct string suffix)
{
    // TODO make this work with windows
    if (str_equals(suffix, STR("../"))) {
        if (str_equals(path, STR("/")))
            return path;
        while (str_ends_with(path, STR("/"))) // trim tailing slashes
            path = str_erase(path, -1, 1);
        int pos = str_find_last(path, STR("/"));
        path = str_erase(path, pos + 1, STR_END);
    } else {
        if (!str_ends_with(path, STR("/")))
            path = str_append(path, STR("/"));
        path = str_append(path, suffix);
    }
    return path;
}

static bool fb_isdir(struct string name)
{
    return str_ends_with(name, STR("/"));
}

static struct string fb_get_selected(struct filebrowser* fb)
{
    int selected = fb->file_list->detail.list.selected;
    return (selected >= 0 && selected < fb->list.size) ?
           fb->list.mem[selected] :
           STR("");
}

static bool fb_event_list(struct widget* w, const SDL_Event* e, const SDL_Rect* rect)
{
    struct filebrowser* fb = w->payload;
    if (e->type != SDL_MOUSEBUTTONDOWN || !widget_changed(w))
        return false;
    struct string name = fb_get_selected(fb);
    if (e->button.timestamp < fb->timestamp + 300 && fb_isdir(name)) { // detect double click
        fb->dir = fb_change_path(fb->dir, name);
        fb_start_scan(fb);
    } else if (fb->type == FILEBROWSER_SAVE && !fb_isdir(name)) {
        widget_set_text(fb->name_edit, str_duplicate(name), true);
    }
    fb->timestamp = e->button.timestamp;
    return true;
}

static bool fb_event_cancel(struct widget* w, const SDL_Event* e, const SDL_Rect* rect)
{
    if (!widget_changed(w))
        return false;
    fb_free(w->payload);
    return true;
}

static bool fb_event_open(struct widget* w, const SDL_Event* e, const SDL_Rect* rect)
{
    if (!widget_changed(w))
        return false;
    struct filebrowser* fb = w->payload;
    struct string name = fb->type != FILEBROWSER_SAVE ?
        fb_get_selected(fb) :
        fb->name_edit->detail.text;
    fb->dir = fb_change_path(fb->dir, name);
    if (fb->callback)
        fb->callback(fb->dir, fb->payload);
    fb_free(fb);
    return true;
}

static bool fb_event_window(struct widget* w, const SDL_Event* e, const SDL_Rect* rect)
{
    struct filebrowser* fb = w->payload;
    // TODO catch ctrl-h for hidden files
    // the scan thread finished
    if (e->type == SDL_USEREVENT && e->user.code == FILEBROWSER_NOTIFY) {
        SDL_WaitThread(fb->thread, NULL);
        widget_set_list(fb->file_list, fb->list, true);
        widget_set_flags(fb->window, 0, WIDGET_ARMED);
        return true;
    }
    // close event
    if (e->type == SDL_WINDOWEVENT && e->window.event == SDL_WINDOWEVENT_CLOSE) {
        fb_free(w->payload);
        return true;
    }
    return false;
}

void filebrowser_show(struct string path, void (*callback) (struct string, void*), void* payload, int type)
{
    assert(type >= FILEBROWSER_OPEN && type <= FILEBROWSER_SAVE);
    struct string title[3] = {STR("Open File"), STR("Select Directory"), STR("Save File")};
    struct string open_text[3] = {STR("Open"), STR("Select"), STR("Save")};

    struct filebrowser* fb = calloc(1, sizeof *fb);
    TRACE_ALOC(fb);
    fb->type = type;
    fb->dir = str_own(SDLX_RealPath(str_cstr(path)));
    fb->callback = callback;
    fb->payload = payload;

    fb->window = widget_new(WIDGET_WINDOW, SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, 533, 400, NULL);
    widget_set_text(fb->window, title[type], false);
    widget_set_event(fb->window, fb_event_window, fb);

    if (type == FILEBROWSER_SAVE)
        fb->name_edit = widget_new(WIDGET_EDIT, 5, 5, -5, 25, fb->window);

    fb->file_list = widget_new(WIDGET_LIST,  5, type == FILEBROWSER_SAVE ? 35 : 5, -5, -35, fb->window);
    widget_set_event(fb->file_list, fb_event_list, fb);

    fb->open_button = widget_new(WIDGET_BUTTON, -210, -30, 100, 25, fb->window);
    widget_set_text(fb->open_button, open_text[type], false);
    widget_set_event(fb->open_button, fb_event_open, fb);

    fb->cancel_button = widget_new(WIDGET_BUTTON, -105, -30, 100, 25, fb->window);
    widget_set_text(fb->cancel_button, STR("Cancel"), false);
    widget_set_event(fb->cancel_button, fb_event_cancel, fb);

    fb_start_scan(fb);
}
