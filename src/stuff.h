#ifndef KE2K_STUFF_H
#define KE2K_STUFF_H

#include <stdint.h>
#include <stdio.h>

static inline int imin(int a, int b)
{
    return a < b ? a : b;
}

static inline int imax(int a, int b)
{
    return a > b ? a : b;
}

static inline int iclamp(int min, int value, int max)
{
    return value < min ? min : value > max ? max : value;
}

static inline float fclampf(float min, float value, float max)
{
    return value < min ? min : value > max ? max : value;
}

#define COUNTOF(x) (sizeof (x) / sizeof(x)[0])

// some compiler specific macros
#ifdef _MSVCVER
    #define STATIC_THREAD_LOCAL __declspec(thread)
    #define WARN_UNUSED
    #define FORMAT_WARN(f, a)
#else
    // assuming gcc
    #define STATIC_THREAD_LOCAL static __thread
    #define WARN_UNUSED         __attribute__((warn_unused_result))
    #define FORMAT_WARN(f, a)   __attribute__((format (printf, f, a)))
#endif

// hash.c
uint32_t city_hash32(const char *s, size_t len);

// filesystem.c
// i prefixed those functions with SDLX cause i think they should be part of sdl filesystem :)
enum {
    SDLX_FILESYSTEM_OTHER       = 0,    // type
    SDLX_FILESYSTEM_DIRECTORY   = 1,    // type
    SDLX_FILESYSTEM_FILE        = 2,    // type
    SDLX_FILESYSTEM_HIDDEN      = 1     // flags
};

typedef struct {
    void*       handle;
    const char* name;   // don't free
    int         type;
    int         flags;
} SDLX_Dirent;

// return 0 on success, -1 on error
int SDLX_FirstDir(SDLX_Dirent* dir, const char* path);
int SDLX_NextDir(SDLX_Dirent* dir);
void SDLX_CloseDir(SDLX_Dirent* dir);

// returns the cannonical absolute path or NULL on error
// for example "." would expand to something like "/home/user/workdir"
// return value must be freed with free()
char* SDLX_RealPath(const char* path);

// debugging macros to trace values. S string, I int, P double, P pointer
#define TRACE()     printf("%s:%d %s()\n", __FILE__, __LINE__, __func__)
#define TRACES(x)   printf("%s:%d %s() %s=%s\n", __FILE__, __LINE__, __func__, #x, x)
#define TRACEI(x)   printf("%s:%d %s() %s=%d\n", __FILE__, __LINE__, __func__, #x, x)
#define TRACEF(x)   printf("%s:%d %s() %s=%f\n", __FILE__, __LINE__, __func__, #x, x)
#define TRACEP(x)   printf("%s:%d %s() %s=%p\n", __FILE__, __LINE__, __func__, #x, (void*) x)

#if 0 // turn on to trace resource allocation and release
    #define TRACE_ALOC(x) printf("+ %p %s:%d\n", (void*) x, __FILE__, __LINE__)
    #define TRACE_FREE(x) if (x) printf("- %p %s:%d\n", (void*) x, __FILE__, __LINE__)
#else
    #define TRACE_ALOC(x)
    #define TRACE_FREE(x)
#endif

#endif // KE2K_STUFF_H
