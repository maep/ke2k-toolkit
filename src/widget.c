#include <assert.h>
#include "stuff.h"
#include "widget.h"

struct font_cache {
    TTF_Font*               font;
    uint64_t                hash;
    struct font_cache*      next;
};

// i deliberately didn't do the classic vtable thing. instead I implemented
// the calls as jump tables. i'm not sure they are better but i wantet to try it.
struct widget_calls {
    void (*event) (struct widget*, const SDL_Event*, const SDL_Rect*);
    void (*paint) (struct widget*, SDL_Renderer*, const SDL_Rect*);
    void (*free)  (struct widget*);
};

static struct widget_calls  call_table[8];  // 8 widgets
static struct widget*       root_window;    // first window created
static struct widget*       active_window;  // window getting events
static TTF_Font*            default_font;   // font used for new widgets
static struct font_cache*   font_cache;     // cache of various fonts

enum { RR = 0, GG = 96, BB = 125 };
static const SDL_Color color1       = {RR, GG, BB, 255};
static const SDL_Color color2       = {RR / 2, GG / 2, BB / 2, 255};
static const SDL_Color bg_color     = {28, 30, 30, 255};
static const SDL_Color text_color   = {255, 255, 255, 255};

// test functions
static inline bool widget_armed(const struct widget* w) { return w->flags & WIDGET_ARMED; }
static inline bool widget_dirty(const struct widget* w) { return w->flags & WIDGET_DIRTY; }
static inline bool widget_pressed(const struct widget* w) { return w->flags & WIDGET_PRESSED; }
static inline bool widget_mouseover(const struct widget* w) { return w->flags & WIDGET_MOUSEOVER; }
static inline bool widget_haschild(const struct widget* w) { return w->type >= WIDGET_PANEL && w->detail.panel.child; }

// returns true if a mouse event e is over r
static bool is_mouse_over_rect(const SDL_Event* e, const SDL_Rect* r)
{
    SDL_Point p = {0, 0};
    switch (e->type) {
    case SDL_MOUSEMOTION:
        p = (SDL_Point) {e->motion.x, e->motion.y};
        break;
    case SDL_MOUSEBUTTONDOWN:
    case SDL_MOUSEBUTTONUP:
        p = (SDL_Point) {e->button.x, e->button.y};
        break;
    case SDL_MOUSEWHEEL:
        SDL_GetMouseState(&p.x, &p.y);
        break;
    default:
        return false;
    }
    return (p.x >= r->x) && (p.x < r->x + r->w) && (p.y >= r->y) && (p.y < r->y + r->h);
}

// clears cached texture and marks the widget as dirty
static void widget_free_texture(struct widget* w)
{
    SDL_DestroyTexture(w->texture);
    w->texture = NULL;
    widget_set_flags(w, WIDGET_DIRTY, 0);
}

// calculates the position of w, relative to r. w's rect can have negative values in which case 
// they are substracted from r's x/y/width/height
static SDL_Rect widget_absolute_rect(const struct widget* w, const SDL_Rect* r)
{
    int x = r->x + w->rect.x + (w->rect.x < 0 ? r->w : 0);
    int y = r->y + w->rect.y + (w->rect.y < 0 ? r->h : 0);
    int v = w->rect.w + (w->rect.w < 0 ? r->w - x + r->x: 0);
    int h = w->rect.h + (w->rect.h < 0 ? r->h - y + r->y: 0);
    return (SDL_Rect) {x, y, v, h};
}

static void render_rect(SDL_Renderer* rend, const SDL_Rect* rect, const SDL_Color* color, bool fill)
{
    SDL_SetRenderDrawColor(rend, color->r, color->g, color->b, color->a);
    if (fill)
        SDL_RenderFillRect(rend, rect);
    else
        SDL_RenderDrawRect(rend, rect);
}

static SDL_Rect widget_paint_text(struct widget* w, SDL_Renderer* rend, struct string text, const SDL_Rect* rect)
{
    int fh = TTF_FontHeight(w->font);
    if (!w->texture) {
        if (str_len(text) == 0)
            return (SDL_Rect) {rect->x + 2, rect->y + (rect->h - fh) / 2, 0, fh};
        SDL_Surface* s = TTF_RenderUTF8_Blended(w->font, str_cstr(text), text_color);
        w->texture = SDL_CreateTextureFromSurface(rend, s);
        SDL_SetTextureBlendMode(w->texture, SDL_BLENDMODE_ADD); // required for sw renderer
        SDL_FreeSurface(s);
    }
    uint32_t fmt;
    int acc, tw, th;
    SDL_QueryTexture(w->texture, &fmt, &acc, &tw, &th);
    // TODO: still assumes texture height < widget height
    SDL_Rect src_rect = {0, 0, imin(rect->w - 2, tw), imin(rect->h, th)};
    SDL_Rect dst_rect = {rect->x + 2, rect->y + (rect->h - fh) / 2,  src_rect.w, src_rect.h};
    SDL_RenderCopy(rend, w->texture, &src_rect, &dst_rect);
    return dst_rect;
}

static void widget_paint_button(struct widget* w, SDL_Renderer* rend, const SDL_Rect* rect)
{
    assert(w->type == WIDGET_BUTTON || w->type == WIDGET_TOGGLE); // w->text requires button type
    const SDL_Color* color = w->flags & WIDGET_PRESSED ? &color2 : &color1;
    render_rect(rend, rect, color, true);
    widget_paint_text(w, rend, w->detail.text, rect);
    if (w->type == WIDGET_TOGGLE) {
        SDL_Rect box = {rect->x + rect->w - 12, rect->y + (rect->h - 8) / 2, 8, 8};
        render_rect(rend, &box, &text_color, w->flags & WIDGET_TOGGLEON);
    }
}

static void widget_paint_edit(struct widget* w, SDL_Renderer* rend, const SDL_Rect* rect)
{
    assert(w->type == WIDGET_EDIT); // w->text requires edit type
    render_rect(rend, rect, &bg_color, true);
    SDL_Rect r = widget_paint_text(w, rend, w->detail.text, rect);
    r = (SDL_Rect) {r.x + r.w, r.y, 1, r.h};
    if (widget_armed(w) && r.x < rect->x + rect->w)
        render_rect(rend, &r, &text_color, false);
    render_rect(rend, rect, &color1, false);
}

static void widget_paint_control(struct widget* w, SDL_Renderer* rend, const SDL_Rect* rect)
{
    assert(w->type == WIDGET_CONTROL); // w->control requires control type
    render_rect(rend, rect, &bg_color, true);
    float fill = (w->detail.control.value - w->detail.control.min) /
                 (w->detail.control.max - w->detail.control.min);
    SDL_Rect r = {rect->x, rect->y, roundf(rect->w * fill), rect->h};
    render_rect(rend, &r, &color2, true);
    char buf[32] = {0};
    if (!w->texture)
        snprintf(buf, sizeof buf - 1, w->flags & WIDGET_CONTROLINT ? "%.f" : "%.2f",
                 w->detail.control.value);
    widget_paint_text(w, rend, str_wrap(buf), rect);
    render_rect(rend, rect, &color1, false);
}

static void widget_create_list_texture(struct widget* w, SDL_Renderer* rend, const SDL_Rect* rect)
{
    SDL_Surface* s = NULL;
    int fh = TTF_FontHeight(w->font);
    for (int i = 0; i < w->detail.list.list.size; i++) {
        const char* str = str_cstr(w->detail.list.list.mem[i]);
        SDL_Surface* sur = TTF_RenderUTF8_Blended(w->font, str, text_color);
        if (!s) {
            SDL_PixelFormat* f = sur->format;
            SDL_Rect r = {0, 0, rect->w, fh * w->detail.list.list.size};
            s = SDL_CreateRGBSurface(0, r.w, r.h, 32, f->Rmask, f->Gmask, f->Bmask, f->Amask);
            SDL_FillRect(s, &r, SDL_MapRGBA(f, 0, 0, 0 , 255));
            SDL_SetSurfaceBlendMode(s, SDL_BLENDMODE_ADD);
        }
        SDL_Rect r = {2, i * fh, sur->w, sur->h};
        SDL_BlitSurface(sur, NULL, s, &r);
        SDL_FreeSurface(sur);
    }
    w->texture = SDL_CreateTextureFromSurface(rend, s);
    SDL_FreeSurface(s);
}

static void widget_paint_list_area(struct widget* w, SDL_Renderer* rend, const SDL_Rect* rect)
{
    // draw selection
    int fh = TTF_FontHeight(w->font);
    SDL_Rect r0 = {rect->x, rect->y - w->detail.list.scroll + fh * w->detail.list.selected, rect->w, fh};
    SDL_Rect r1 = {0, 0, 0, 0};
    if (SDL_IntersectRect(&r0, rect, &r1))
        render_rect(rend, &r1, &color2, true);
    if (!w->texture)
        widget_create_list_texture(w, rend, rect);
    // draw texture
    uint32_t fmt;
    int acc, tw, th;
    SDL_QueryTexture(w->texture, &fmt, &acc, &tw, &th);
    r0 = (SDL_Rect) {0, w->detail.list.scroll, imin(rect->w, tw), imin(rect->h, th)};
    r1 = (SDL_Rect) {rect->x, rect->y, r0.w, r0.h};
    SDL_RenderCopy(rend, w->texture, &r0, &r1);
    // draw scroll indicator
    int hb = imax(1, rect->h * rect->h / th);
    if (hb < rect->h) {
        int yb = (w->detail.list.scroll * rect->h) / th;
        r0 = (SDL_Rect) {rect->x + rect->w - 2, rect->y + yb, 1, hb};
        render_rect(rend, &r0, &text_color, true);
    }
}

static void widget_paint_list(struct widget* w, SDL_Renderer* rend, const SDL_Rect* rect)
{
    assert(w->type == WIDGET_LIST); // w->detail.list requires list type
    render_rect(rend, rect, &bg_color, true);
    if (w->detail.list.list.size)
        widget_paint_list_area(w, rend, rect);
    render_rect(rend, rect, &color1, false);
}

static void widget_paint_panel(struct widget* w, SDL_Renderer* rend, const SDL_Rect* rect)
{
    assert(w->type >= WIDGET_PANEL); // w->detail.child requires panel/window type
    render_rect(rend, rect, &bg_color, true);
    render_rect(rend, rect, &color2, false);
    for (struct widget* c = w->detail.panel.child; c; c = c->next) // force children to redraw
        widget_set_flags(c, WIDGET_DIRTY, 0);
}

static void widget_paint_label(struct widget* w, SDL_Renderer* rend, const SDL_Rect* rect)
{
    assert(w->type == WIDGET_LABEL); // w->text requires label type
    widget_paint_text(w, rend, w->detail.text, rect);
}

static bool widget_paint(struct widget* w, SDL_Renderer* rend, const SDL_Rect* rect)
{
    bool changed = false;
    if (widget_dirty(w)) {
        SDL_Rect r = widget_absolute_rect(w, rect);
        call_table[w->type].paint(w, rend, &r);
        widget_set_flags(w, 0, WIDGET_DIRTY);
        changed = true;
    }
    if (widget_haschild(w)) {
        SDL_Rect r = widget_absolute_rect(w, rect);
        for (w = w->detail.panel.child; w; w = w->next)
            changed |= widget_paint(w, rend, &r);
    }
    return changed;
}

void widget_paint_all(void)
{
    if (!active_window)
        return;
    SDL_Renderer* rend = SDL_GetRenderer(active_window->detail.panel.window);
    if (widget_paint(active_window, rend, &active_window->rect))
        SDL_RenderPresent(rend);
}

static void widget_event_button(struct widget* w, const SDL_Event* e, const SDL_Rect* rect)
{
    switch (e->type) {
    case SDL_MOUSEMOTION:
        if (widget_armed(w) && (widget_mouseover(w) != widget_pressed(w)))
            widget_set_flags(w, WIDGET_DIRTY | (~w->flags & WIDGET_PRESSED), w->flags & WIDGET_PRESSED);
        break;
    case SDL_MOUSEBUTTONDOWN:
        widget_set_flags(w, WIDGET_DIRTY | WIDGET_ARMED | WIDGET_PRESSED, 0);
        break;
    case SDL_MOUSEBUTTONUP:
        if (widget_mouseover(w) && widget_armed(w))
            widget_set_flags(w, WIDGET_CHANGED, 0);
        if (w->type == WIDGET_TOGGLE && widget_mouseover(w) && widget_armed(w))
            widget_set_flags(w, ~w->flags & WIDGET_TOGGLEON, w->flags & WIDGET_TOGGLEON);
        widget_set_flags(w, WIDGET_DIRTY, WIDGET_ARMED | WIDGET_PRESSED);
        break;
    }
}

static void widget_event_edit(struct widget* w, const SDL_Event* e, const SDL_Rect* rect)
{
    assert(w->type == WIDGET_EDIT);
    switch (e->type) {
    case SDL_MOUSEBUTTONDOWN:
        if (widget_mouseover(w) && !widget_armed(w)) {
            SDL_SetTextInputRect((SDL_Rect*)rect); // const cast, probably an api bug
            SDL_StartTextInput();
            widget_set_flags(w, WIDGET_DIRTY | WIDGET_ARMED, 0);
        } else if (~widget_mouseover(w) && widget_armed(w)) {
            SDL_StopTextInput();
            widget_set_flags(w, WIDGET_DIRTY | WIDGET_CHANGED, WIDGET_ARMED);
        }
        break;
    case SDL_KEYDOWN:
        if (e->key.keysym.sym == SDLK_BACKSPACE && str_len(w->detail.text)) {
            // look for the last utf8 byte not matching 0b10xxxxxx
            int chlen = 1, len = str_len(w->detail.text);
            char* buf = str_buf(w->detail.text);
            for (; ((int)buf[len - chlen] & 192) == 128 && chlen < len; chlen++) {}
            w->detail.text = str_erase(w->detail.text, -chlen, STR_END);
            widget_set_flags(w, WIDGET_DIRTY, 0);
            widget_free_texture(w);
        }
        if (e->key.keysym.sym == SDLK_RETURN)
            widget_set_flags(w, WIDGET_DIRTY | WIDGET_CHANGED, WIDGET_ARMED);
        break;
    case SDL_TEXTINPUT:
        w->detail.text = str_cap(w->detail.text) ?
            str_append(w->detail.text, str_wrap(e->text.text)) :
            str_new(e->text.text);
        widget_free_texture(w); // sets dirty flag
        break;
    }
}

static void widget_set_value_from_x(struct widget* w, int x, const SDL_Rect* r)
{
    assert(w->type == WIDGET_CONTROL);
    float tmp = (w->detail.control.max - w->detail.control.min) *
                (x - r->x) / r->w + w->detail.control.min;
    tmp = fclampf(w->detail.control.min, tmp, w->detail.control.max);
    tmp = w->flags & WIDGET_CONTROLINT ? roundf(tmp) : tmp;
    tmp = tmp < 0.f ? tmp : fabsf(tmp); // get rid of -0
    if (tmp != w->detail.control.value) {
        w->detail.control.value = tmp;
        widget_free_texture(w); // sets dirty flag
        widget_set_flags(w, WIDGET_CHANGED, 0);
    }
}

static void widget_event_control(struct widget* w, const SDL_Event* e, const SDL_Rect* rect)
{
    switch (e->type) {
    case SDL_MOUSEBUTTONDOWN:
        widget_set_value_from_x(w, e->button.x, rect);
        widget_set_flags(w, WIDGET_ARMED, 0);
        break;
    case SDL_MOUSEBUTTONUP:
        widget_set_flags(w, 0, WIDGET_ARMED);
        break;
    case SDL_MOUSEMOTION:
        if (widget_armed(w))
            widget_set_value_from_x(w, e->motion.x, rect);
        break;
    }
}

static void widget_event_list(struct widget* w, const SDL_Event* e, const SDL_Rect* rect)
{
    assert(w->type == WIDGET_LIST);
    int tmp = 0;
    switch (e->type) {
    case SDL_MOUSEBUTTONDOWN:
        // calculate which item would be selected
        tmp = (w->detail.list.scroll + e->button.y - rect->y) / TTF_FontHeight(w->font);
        if (tmp >= 0 && tmp < w->detail.list.list.size) {
            w->detail.list.selected = tmp;
            widget_set_flags(w, WIDGET_DIRTY | WIDGET_CHANGED, 0);
        }
        break;
    case SDL_MOUSEWHEEL:
        // calculate max possible scroll
        tmp = imax(w->detail.list.list.size * TTF_FontHeight(w->font) - rect->h, 0);
        tmp = iclamp(0, w->detail.list.scroll - e->wheel.y * 3, tmp); // apply scroll
        if (tmp != w->detail.list.scroll) {
            w->detail.list.scroll = tmp;
            widget_set_flags(w, WIDGET_DIRTY, 0);
        }
        break;
    }
}

static void widget_event_window(const SDL_Event* e)
{
    // find panel matching event window id
    for (active_window = root_window; active_window &&
         e->window.windowID != SDL_GetWindowID(active_window->detail.panel.window);
         active_window = active_window->next) {}
    if (!active_window) // happens when window is deleted
        return;
    switch (e->window.event) {
    case SDL_WINDOWEVENT_RESIZED:
        active_window->rect.w = e->window.data1;
        active_window->rect.h = e->window.data2;
    case SDL_WINDOWEVENT_EXPOSED:
        widget_set_flags(active_window, WIDGET_DIRTY, 0);
    }
    if (active_window->event)
        active_window->event(active_window, e, &active_window->rect);
}

// this function returns true if the event was consumed. consumed events are not
// not processes any further. this is important if a widget is freed by it's own
// event handler
static bool widget_event(struct widget* w, const SDL_Event* e, const SDL_Rect* rect)
{

    SDL_Rect r = widget_absolute_rect(w, rect);
    unsigned over = is_mouse_over_rect(e, &r) ? ~0 : 0;
    widget_set_flags(w, WIDGET_MOUSEOVER & over, WIDGET_MOUSEOVER & ~over);
    if (over || widget_armed(w)) {
        if (call_table[w->type].event)
            call_table[w->type].event(w, e, &r);
        if (w->event && w->event(w, e, &r))
            return true;
    }
    if (widget_haschild(w)) {
        for (w = w->detail.panel.child; w; w = w->next) {
            if (widget_event(w, e, &r))
                return true;
        }
    }
    return false;
}

void widget_event_all(const SDL_Event* event)
{
    if (event->type == SDL_WINDOWEVENT) // window events need special treatment
        widget_event_window(event);     // widgets don't need to see window events (for now?)
    else
        widget_event(active_window, event, &active_window->rect);
}

static void widget_add(struct widget** first, struct widget* w)
{
    for (; *first; first = &(*first)->next) {}
    *first = w;
}

static void widget_remove(struct widget** first, struct widget* w)
{
    for (; *first && *first != w; first = &(*first)->next) {}
    *first = (*first)->next;
}

struct widget* widget_new(int type, int x, int y, int width, int height, struct widget* parent)
{
    struct widget* w = calloc(1, sizeof *w);
    TRACE_ALOC(w);
    w->type = type;
    w->font = default_font;
    if (type == WIDGET_WINDOW) {
        assert(width > 0 && height > 0);
        w->rect = (SDL_Rect) {0, 0, width, height};
        w->detail.panel.window = SDL_CreateWindow("", x, y, width, height, SDL_WINDOW_RESIZABLE);
        TRACE_ALOC(w->detail.panel.window);
        // the software renderer is a lot faster on my crappy poulsbo netbook
        SDL_CreateRenderer(w->detail.panel.window, -1, SDL_RENDERER_SOFTWARE);
        widget_add(&root_window, w);
        widget_set_flags(w, WIDGET_FREEDETAIL | WIDGET_DIRTY, 0);
    } else {
        assert(parent && parent->type >= WIDGET_PANEL);
        w->rect = (SDL_Rect) {x, y, width, height};
        if (type == WIDGET_LIST)
            w->detail.list.selected = -1;
        widget_add(&parent->detail.panel.child, w);
        widget_set_flags(w, WIDGET_DIRTY, 0);
    }
    return w;
}

static void widget_free_window(struct widget* w)
{
    assert(w->type == WIDGET_WINDOW);
    widget_remove(&root_window, w);
    if (w->flags & WIDGET_FREEDETAIL) {
        SDL_DestroyRenderer(SDL_GetRenderer(w->detail.panel.window)); // is this needed?
        SDL_DestroyWindow(w->detail.panel.window);
        TRACE_FREE(w->detail.panel.window);
    }
    if (w == active_window)
        active_window = NULL; // otherwise widget_paint_all() will poop in it's pants
}

static void widget_free_text(struct widget* w)
{
    assert(w->type >= WIDGET_BUTTON && w->type <= WIDGET_LABEL);
    if (w->flags & WIDGET_FREEDETAIL)
        str_free(w->detail.text);
}

static void widget_free_list(struct widget* w)
{
    assert(w->type == WIDGET_LIST);
    if (w->flags & WIDGET_FREEDETAIL)
        string_list_free_all(&w->detail.list.list);
}

void widget_free(struct widget* w)
{
    if (!w)
        return;
    if (call_table[w->type].free)
        call_table[w->type].free(w);
    SDL_DestroyTexture(w->texture);
    memset(w, 0, sizeof *w); // make sure use-after-free causes segfaults of paiiinc
    free(w);
    TRACE_FREE(w);
}

void widget_free_all(struct widget* w)
{
    if (w && widget_haschild(w)) {
        struct widget* i = w->detail.panel.child;
        for (struct widget* tmp = NULL; i; i = tmp) {
            tmp = i->next;
            widget_free_all(i);
        }
    }
    widget_free(w);
}

static TTF_Font* fontcache_load(struct string path, int size)
{
    struct font_cache** fc = &font_cache;
    uint32_t hash = str_hash(path) ^ size;
    for (; *fc; fc = &(*fc)->next)
        if ((*fc)->hash == hash)
            break;
    if (!*fc) {
        *fc = calloc(1, sizeof **fc);
        TRACE_ALOC(*fc);
        (*fc)->font = TTF_OpenFont(str_cstr(path), size);
        assert((*fc)->font);
        TTF_SetFontHinting((*fc)->font, TTF_HINTING_NONE);
    }
    return (*fc)->font;
}

void widget_set_default_font(struct string path, int size)
{
    default_font = fontcache_load(path, size);
}

void widget_set_font(struct widget* w, struct string path, int size)
{
    w->font = fontcache_load(path, size);
    widget_free_texture(w);
}

void widget_set_event(struct widget* w,
                      bool (*event) (struct widget*, const SDL_Event*, const SDL_Rect*),
                      void* payload)
{
    w->event = event;
    w->payload = payload;
}

void widget_set_text(struct widget* w, struct string text, bool free_string)
{
    if (w->type == WIDGET_WINDOW) {
        SDL_SetWindowTitle(w->detail.panel.window, str_cstr(text));
    } else {
        assert(w->type >= WIDGET_BUTTON && w->type <= WIDGET_LABEL);
        widget_free_text(w);
        w->detail.text = text;
        widget_set_flags(w, free_string ? WIDGET_FREEDETAIL : 0, free_string ? 0 : WIDGET_FREEDETAIL);
        widget_free_texture(w); // sets dirty flag
    }
}

void widget_set_range(struct widget* w, float min, float max, bool integers)
{
    assert(w->type == WIDGET_CONTROL);
    w->detail.control.min = min;
    w->detail.control.max = max;
    if (integers)
        widget_set_flags(w, WIDGET_CONTROLINT, 0);
    widget_free_texture(w); // sets dirty flag
}

void widget_set_value(struct widget* w, float value)
{
    assert(w->type == WIDGET_CONTROL);
    w->detail.control.value = fclampf(w->detail.control.min, value, w->detail.control.max);
}

float widget_get_value(struct widget* w)
{
    assert(w->type == WIDGET_CONTROL);
    return w->detail.control.value;
}

void widget_set_list(struct widget* w, struct string_list list, bool free_list)
{
    assert(w->type == WIDGET_LIST);
    widget_free_list(w);
    w->detail.list.list = list;
    w->detail.list.scroll = 0;
    w->detail.list.selected = -1;
    widget_set_flags(w, free_list ? WIDGET_FREEDETAIL : 0, free_list ? 0 : WIDGET_FREEDETAIL);
    widget_free_texture(w); // sets dirty flag
}

bool widget_changed(struct widget* w)
{
    bool changed = w->flags & WIDGET_CHANGED;
    widget_set_flags(w, 0, WIDGET_CHANGED);
    return changed;
}

// be carefull the order matches the enum
static struct widget_calls call_table[8] = {
    {widget_event_button,   widget_paint_button,    widget_free_text},  // button
    {widget_event_button,   widget_paint_button,    widget_free_text},  // toggle
    {widget_event_edit,     widget_paint_edit,      widget_free_text},  // edit
    {NULL,                  widget_paint_label,     widget_free_text},  // label
    {widget_event_list,     widget_paint_list,      widget_free_list},  // list
    {widget_event_control,  widget_paint_control,   NULL},              // control
    {NULL,                  widget_paint_panel,     NULL},              // panel
    {NULL,                  widget_paint_panel,     widget_free_window} // window
};
