#ifndef KE2K_WIDGET_H
#define KE2K_WIDGET_H

#include <SDL.h>
#include <SDL_ttf.h>
#include "str.h"
#include "container.h"

// i tried to create a minimal widget toolkit. this was meant to be a learning experience rather than to
// create something usefull. thus sometimes i opted to use a unusual approach instead of maybe usint the
// obious solution. the goal was to be minimalistic, but sometimes that was not possible as much as i hoped.

// all widgets share a common struct but some have special data needs and use the detail union for that. the
// widgets are stored in a tree. regualar widgets have siblings, and are the only ones to know of it. only panels
// and windows can have children and siblings. thus, normal widgets can be thought of linked list elements, panels
// and windows act as tree nodes.

// the widget's internal state is stored in the flags member and is changed by event handlers and paint functions.
// the user usually only looks at the flags, and doesn't need to change them. the user may set the event handler for
// which will recieve all events the widget does. only mouse events over the widgets are passed by default, but
// setting the WIDGET_ARMED flag will cause all events to be passed. user are free to store a pointer in the payload
// member. this will never be used by the widgets themselves.

// some setter functions offer to free the passed resource once the widget is freed or if the setter is called
// again.

// currently there is no support for user widgets, that is planned for the future, likely by adding paint and free
// callbacks.

enum widget_type {
    WIDGET_BUTTON       = 0,            // your plain old button
    WIDGET_TOGGLE,                      // button with on/off state (WIDGET_TOGGLE_ON)
    WIDGET_EDIT,                        // single line text editor
    WIDGET_LABEL,                       // static text display
    WIDGET_LIST,                        // a list
    WIDGET_CONTROL,                     // control for numeric value
    WIDGET_PANEL,                       // widget that contains other widgets
    WIDGET_WINDOW                       // top level window
};

enum widget_flag {
    WIDGET_ARMED        = 1 << 0,       // widget recieves input events even if mouse is somewhere else
    WIDGET_PRESSED      = 1 << 1,       // widget is in the pressed state
    WIDGET_DIRTY        = 1 << 2,       // widget needs a redraw
    WIDGET_FREEDETAIL   = 1 << 3,       // free any resources like text or window
    WIDGET_TOGGLEON     = 1 << 4,       // toggle is toggled
    WIDGET_MOUSEOVER    = 1 << 5,       // set if last mouse event was over widget
    WIDGET_CONTROLINT   = 1 << 6,       // control round use int
    WIDGET_CHANGED      = 1 << 7        // widget has new data, see widget_changed()
};

enum filebrowser_mode {
    FILEBROWSER_OPEN            = 0,
    FILEBROWSER_OPENDIR         = 1,
    FILEBROWSER_SAVE            = 2
};

// members users can change: flags, event, paint, payload
struct widget {
    int16_t             type;
    int16_t             flags;
    SDL_Rect            rect;
    SDL_Texture*        texture;
    TTF_Font*           font;
    struct widget*      next;
    bool                (*event) (struct widget*, const SDL_Event*, const SDL_Rect*);
    void*               payload;
    union detail {                  // anonymous unions require c11 unfortunately
        struct string   text;
        struct {
            struct widget*      child;
            SDL_Window*         window;
        } panel;
        struct {
            float               value;
            float               min;
            float               max;
        } control;
        struct {
            struct string_list  list;
            int                 selected;
            int                 scroll;
        } list;
    } detail;
};

// create new widget and add to parents
// negative x/y anchor widget position to parent's right/bottom
// negative width/height anchor widget size to parent's right/bottom
struct widget* widget_new(int type, int x, int y, int width, int height, struct widget* parent);

void widget_free(struct widget* w);

// like free_widget but goes through the whole tree
void widget_free_all(struct widget* widget);

// applies to all widgets
// set event handler and user payload for widget.
void widget_set_event(struct widget* widget,
                      bool (*event) (struct widget*, const SDL_Event*, const SDL_Rect*),
                      void* payload);

// applies to button, toggle, label, edit, window widgets
// set free_string to true to have widget_free free the string
void widget_set_text(struct widget* widget, struct string text, bool free_string);

// set default font for widgets created by widget_new()
void widget_set_default_font(struct string path, int size);

// set widget font, size is height in pixels
void widget_set_font(struct widget* widget, struct string path, int size);

// applies to control widget
void widget_set_range(struct widget* widget, float min, float max, bool integers);
void widget_set_value(struct widget* widget, float value);
float widget_get_value(struct widget* widget);

// applies to list widget
// list must not be freed for lifetime of widget
void widget_set_list(struct widget* widget, struct string_list list, bool free_list);

// this function is called from event handlers to check if a widget has changed in
// a way the user should be aware. calling this function will clear the changed flag.
// button: clicked, toggle: toggle state changed, control: value changed,
// edit: return key or focus loss, (but not key strokes, SDL_TEXTINPUT does that)
// list: selectioin changed
bool widget_changed(struct widget* widget);

// paint all windows
void widget_paint_all(void);

// dispatch event to all windows
void widget_event_all(const SDL_Event* event);

// show a file browers dialog window. will be freed automatically upon close. upon succsessfull selection
// the callback function is called, with the selected path and the user payload.
void filebrowser_show(struct string path, void (*callback) (struct string, void*), void* payload, int flags);

// function to change widget flags. set_flags are set, and clear_flags are cleared.
// there is no get_flags. just use if (w->flags & FLAG_TO_TEST) {...}.
static inline void widget_set_flags(struct widget* w, int set_flags, int clear_flags)
{
    w->flags = set_flags | (w->flags & ~clear_flags);
}

#endif
