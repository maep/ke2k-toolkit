#include <stdlib.h>
#include <string.h>
#include "container.h"
#include "stuff.h"

void string_list_free(struct string_list* list)
{
    free(list->mem);
    TRACE_FREE(list->mem);
    memset(list, 0, sizeof *list);
}

void string_list_free_all(struct string_list* list)
{
    for (int i = 0; i < list->size; i++)
        str_free(list->mem[i]);
    string_list_free(list);
}

// TODO check if it fits into 16 bit int
void string_list_append(struct string_list* list, struct string item)
{
    if (list->size + 1 > list->capacity) {
        list->capacity = imax(list->capacity * 2, 16);
        TRACE_FREE(list->mem);
        list->mem = realloc(list->mem, list->capacity * sizeof item);
        TRACE_ALOC(list->mem);
    }
    list->mem[list->size] = item;
    list->size++;
}

static int string_qsort_cmp(const void* a, const void* b)
{
    return str_compare_i(*(struct string*)a, *(struct string*)b);
}

void string_list_sort(struct string_list* list)
{
    qsort(list->mem, list->size, sizeof *list->mem, string_qsort_cmp);
}
