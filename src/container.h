#ifndef KE2K_CONTAINER_H
#define KE2K_CONTAINER_H

#include "str.h"

struct string_list {
    struct string*  mem;
    uint16_t        size;
    uint16_t        capacity;
};

// add string to end of list
void string_list_append(struct string_list* list, struct string item);
// sort list with quicksort case insensitive
void string_list_sort(struct string_list* list);
// frees memory and zeros list
void string_list_free(struct string_list* list);
// frees memory, all strings in it and zeros list
void string_list_free_all(struct string_list* list);

#endif
