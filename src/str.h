#ifndef KE2K_STR_H
#define KE2K_STR_H

#include <stdbool.h>
#include <stdint.h>
#include "stuff.h"

// a string "class". if can hold const strings and dynamic buffers. the length is stored in the struct.
//
// the struct is meant to be passed around by value, even for functions that change the string. this
// means the user has to do things like s = str_append(s, STR("foo")). it might might seem like a
// bad design decision but it allows chaining of calls like s = str_append(str_erase(s, -1, 1), STR(foo));
// function that operate on indices also accept python like slice notation. in the example above the erase
// would remove the last char in the buffer.
// you can construct strings at compile time with zero overhead with the STR macro. it makes use of compound
// literals for that, and you may use them in function arguments as shown above. it doesn't work as an initializer,
// use the STRI macro in those cases.
// due to size considerations the maxium supported string size is 16 kb on 32 bit machines. it should be
// plenty for most scenarios.

struct string {
    uintptr_t       str;      // don't touch dis
    uintptr_t       siz;      // dito
};

// some constants, STR_END is the only one users should to care about
#if UINTPTR_MAX == UINT32_MAX // 32 bit machine
    #define STR_END     UINT16_MAX
    #define STR_MASK    UINT16_MAX
    #define STR_SHIFT   16
#else
    #define STR_END     INT32_MAX
    #define STR_MASK    UINT32_MAX
    #define STR_SHIFT   32
#endif

// string "constructors" for creating strings with no runtime overhead at compile time
#define STRI(str) {(uintptr_t) (str), (sizeof (str) - 1) & STR_MASK}    // for initializers
#define STR(str) (struct string) STRI(str)                              // for everything else

// create a new string as copy of zero terminated string
// s will not be freed by str_free()
// must be freed with str_free()
struct string str_new(const char* s);

// create an empty string with capacity of at least len + 1
// must be freed with str_free(), buffer is not zeroed
struct string str_new_empty(int capacity);

// create a new string using printf formatting
struct string str_new_format(const char* format, ...) FORMAT_WARN(1, 2);

// free strings allocated with str_duplicate, str_own, str_new, str_new_empty
// it's ok to use with static strings
void str_free(struct string s);

// like str_new(), but will take ownership of s
// s will be freed by str_free()
// must be freed with str_free()
struct string str_own(char* s);

// duplicate string
// must be freed with str_free()
struct string str_duplicate(struct string s);

// wrap a zero terminated string
// freeing has no effect
struct string str_wrap(const char* s);

// wrap a string of length len
// freeing has no effect
struct string str_wrap_n(const char* s, int len);

// returns a const null terminated string. it may be allocated on a thread local buffer, if the internal string
// is not null terminated. in that case it will only be valid until the next call to this function in the same
// thread context.
const char* str_cstr(struct string s);

// return pointer to the internal string buffer. it may not be terminated! use str_len to get length and str_cap
// to get buffer capacity
static inline char* str_buf(struct string s)
{
    return (char*)s.str;
}

// returns the length of string. the zero terminator is not counted.
static inline int str_len(struct string s)
{
    return (int)(s.siz & STR_MASK);
}

// returns capacity of string buffer. if it returns 0 s doesn't 'own' the buffer,
// consider it to be unsafe to write into the buffer
static inline long str_cap(struct string s)
{
    return (int)((s.siz >> STR_SHIFT) & STR_MASK);
}

// write into stirng with printf formatting.
// example:
//      struct string s = str_format_new("%d", 42);         // str_format_new() to create new string
//      s = str_format(s, "there are %d lights", 4);        // str_format() reuses buffer in s
//      str_free(s)
// TODO: printf warnings, warn on unused return value
struct string WARN_UNUSED str_format(struct string s, const char* format, ...) FORMAT_WARN(2, 3);

// appends string to s. actually a wrapper to str_insert(s, string, STR_END).
// example:
//      struct string s = str_new("reticulating");
//      s = str_append(s, STR_S(" splines"));
//      str_free(s);
// bad example:
//      struct string s = str_wrap("i wanna be a mongoose");
//      str_append(s, STR_S("-dog"));       // ERROR
struct string str_append(struct string s, struct string string) WARN_UNUSED;

// insert substring into s at pos. if pos is negative the position is indexed from the back of the string.
struct string WARN_UNUSED str_insert(struct string s, struct string string, int index);

// erase bytes in s. begin and end have same semantics atr str_substring()
struct string WARN_UNUSED str_erase(struct string s, int begin, int length);

// sets s to string. it grows the buffer of s if required and overwrites anything
// that was in s before.
struct string WARN_UNUSED str_set(struct string s, struct string string, int pos);

// create substring, using same buffer as s. if begin is negative the position is indexed from the back of
// the string. same of length, if negative it marks the position from the back of the string. length can
// also be STR_END. this doesn't change s meaning the returned string may not be zero terminated. if you need
// a zero terminated substring, use in compination with str_cstr: str_cstr(str_substring(s, 1, 2));
// freeing has no effect
struct string str_substring(struct string s, int begin, int length);

// search functions ------------------------------

// returns true if s contains what, case sensitive
bool str_contains(struct string haystack, struct string needle);
// returns true if s contains what, not case sensitive
bool str_contains_i(struct string haystack, struct string needle);
// returns STR_END if no match was found, case sensitive
int str_find_first(struct string haystack, struct string needle);
// returns STR_END if no match was found, not case sensitive
int str_find_first_i(struct string haystack, struct string needle);
// returns STR_END if no match was found, case sensitive
int str_find_last(struct string haystack, struct string needle);
// returns STR_END if no match was found, not case sensitive
int str_find_last_i(struct string haystack, struct string needle);


// returns true if s starts with prefix, case sensitive
bool str_starts_with(struct string s, struct string prefix);
// returns true if s starts with prefix, not case sensitive
bool str_starts_with_i(struct string s, struct string prefix);
// returns true if s ends with suffix, case sensitive
bool str_ends_with(struct string s, struct string suffix);
// returns true if s ends with suffix, not case sensitive
bool str_ends_with_i(struct string s, struct string suffix);


// like strcmp, case sensitive
int str_compare(struct string a, struct string b);
// like strcmp, not case sensitive
int str_compare_i(struct string a, struct string b);
// wrapper for str_compare() == 0
bool str_equals(struct string a, struct string b);
// wrapper for str_compare_i() == 0
bool str_equals_i(struct string a, struct string b);


// converts string to lowercase, returned string is same as s
struct string str_to_lower(struct string s);

// return 32 bit hash of string (city_hash32)
uint32_t str_hash(struct string s);

#endif
