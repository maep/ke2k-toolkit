#ifdef _WIN32
    #define WIN32_LEAN_AND_MEAN
    #define _CRT_SECURE_NO_WARNINGS
    #define _CRT_NONSTDC_NO_WARNINGS
    #include <windows.h>
#else
    #define _BSD_SOURCE
    #include <dirent.h>
    #include <stdlib.h>
#endif

#include <string.h>
#include "stuff.h"

#ifdef _WIN32

static void get_find_data(SDLX_Dirent* dir, WIN32_FIND_DATA* find_data)
{
    dir->name = find_data.cFileName;
    dir->type = find_data.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY ? SDLX_FILESYSTEM_DIRECTORY :
                find_data.dwFileAttributes & FILE_ATTRIBUTE_NORMAL ? SDLX_FILESYSTEM_FILE :
                SDLX_FILESYSTEM_OTHER;
}

int SDLX_FirstDir(SDLX_Dirent* dir, const char* path)
{
    WIN32_FIND_DATA find_data;
    TCHAR* search_path;
    int len;

    memset(dir, 0, sizeof *dir);
    len = lstrlen(path);
    search_path = _malloca((len + 3) * sizeof *search_path);
    lstrcpy(search_path, path);
    lstrcpy(search_path + len, "\\*");
    dir->handle = FindFirstFileA(search_path, &find_data);
    _freea(search_path);
    if (dir->handle != INVALID_HANDLE_VALUE)
        return -1;
    get_find_data(dir, &find_data);
    return 0;
}

// you need to free the return value
int SDLX_NextDir(SDLX_Dirent* dir)
{
    WIN32_FIND_DATA find_data;
    if (!dir || !dir->handle)
        return -1;
    if (!FindNextFileA(d->handle, &find_data))
        return -1;
    get_find_data(dir, &find_data);
    return 0;
}

void SDLX_CloseDir(SDLX_Dirent* dir)
{
    if (dir && dir->handle)
        FindClose(dir->handle);
}

#else // LINUX / BSD

int SDLX_FirstDir(SDLX_Dirent* dir, const char* path)
{
    memset(dir, 0, sizeof *dir);
    dir->handle = opendir(path);
    return dir->handle ? SDLX_NextDir(dir) : -1;
}

int SDLX_NextDir(SDLX_Dirent* dir)
{
    struct dirent* dirent;
    if (!dir || !dir->handle)
        return -1;
    dirent = readdir(dir->handle);
    if (!dirent)
        return -1;
    dir->name = dirent->d_name;
    dir->type = dirent->d_type == DT_DIR ? SDLX_FILESYSTEM_DIRECTORY :
                dirent->d_type == DT_REG ? SDLX_FILESYSTEM_FILE :
                SDLX_FILESYSTEM_OTHER;
    dir->flags = 0;
    if (dir->name[0] == '.' && dir->name[1] && dir->name[1] != '.')
        dir->flags |= SDLX_FILESYSTEM_HIDDEN;
    return 0;
}

void SDLX_CloseDir(SDLX_Dirent* dir)
{
    if (dir && dir->handle)
        closedir(dir->handle);
}

char* SDLX_RealPath(const char* path)
{
    return realpath(path, NULL);
}

#endif // POSI
