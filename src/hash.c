// Copyright (c) 2011 Google, Inc.
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//
// CityHash, by Geoff Pike and Jyrki Alakuijala
// C fixes by m.aep@live.com

#include <stdlib.h>
#include <string.h>
#include "stuff.h"

#ifdef _MSC_VER
    #define bswap_32(x) _byteswap_ulong(x)
#elif defined(__APPLE__)
    #include <libkern/OSByteOrder.h>
    #define bswap_32(x) OSSwapInt32(x)
#elif defined(__NetBSD__)
    #include <sys/types.h>
    #include <machine/bswap.h>
    #if defined(__BSWAP_RENAME) && !defined(__bswap_32)
        #define bswap_32(x) bswap32(x)
    #endif
#else
    #include <byteswap.h>
#endif

#if SDL_BYTEORDER == SDL_BIG_ENDIAN
    #define uint32_in_expected_order(x) (bswap_32(x))
#else
    #define uint32_in_expected_order(x) (x)
#endif

static const uint32_t C1 = 0xcc9e2d51;
static const uint32_t C2 = 0x1b873593;
static const uint32_t C3 = 0xe6546b64;

static uint32_t unaligned_load32(const char *p) {
  uint32_t result;
  memcpy(&result, p, sizeof result);
  return result;
}

static uint32_t fetch32(const char *p) {
    return uint32_in_expected_order(unaligned_load32(p));
}

static uint32_t fmix(uint32_t h)
{
    h ^= h >> 16;
    h *= 0x85ebca6b;
    h ^= h >> 13;
    h *= 0xc2b2ae35;
    h ^= h >> 16;
    return h;
}

static uint32_t rotate32(uint32_t val, int shift)
{
    // Avoid shifting by 32: doing so yields an undefined result.
    return shift == 0 ? val : ((val >> shift) | (val << (32 - shift)));
}

static uint32_t mur(uint32_t a, uint32_t h)
{
    a *= C1;
    a = rotate32(a, 17);
    a *= C2;
    h ^= a;
    h = rotate32(h, 19);
    return h * 5 + C3;
}

static uint32_t hash32len13to24(const char *s, size_t len)
{
    uint32_t a = fetch32(s - 4 + (len >> 1));
    uint32_t b = fetch32(s + 4);
    uint32_t c = fetch32(s + len - 8);
    uint32_t d = fetch32(s + (len >> 1));
    uint32_t e = fetch32(s);
    uint32_t f = fetch32(s + len - 4);
    uint32_t h = len;
    return fmix(mur(f, mur(e, mur(d, mur(c, mur(b, mur(a, h)))))));
}

static uint32_t hash32len0to4(const char *s, size_t len)
{
    uint32_t b = 0;
    uint32_t c = 9;
    for (int i = 0; i < len; i++) {
        signed char v = s[i];
        b = b * C1 + v;
        c ^= b;
    }
    return fmix(mur(b, mur(len, c)));
}

static uint32_t hash32len5to12(const char *s, size_t len)
{
    uint32_t a = len, b = len * 5, c = 9, d = b;
    a += fetch32(s);
    b += fetch32(s + len - 4);
    c += fetch32(s + ((len >> 1) & 4));
    return fmix(mur(c, mur(b, mur(a, d))));
}

uint32_t city_hash32(const char *s, size_t len)
{
    if (len <= 4)
        return hash32len0to4(s, len);
    if (len <= 12)
        return hash32len5to12(s, len);
    if (len <= 24)
        return hash32len13to24(s, len);

    uint32_t h = len, g = C1 * len, f = g, tmp = 0;
    uint32_t a0 = rotate32(fetch32(s + len - 4) * C1, 17) * C2;
    uint32_t a1 = rotate32(fetch32(s + len - 8) * C1, 17) * C2;
    uint32_t a2 = rotate32(fetch32(s + len - 16) * C1, 17) * C2;
    uint32_t a3 = rotate32(fetch32(s + len - 12) * C1, 17) * C2;
    uint32_t a4 = rotate32(fetch32(s + len - 20) * C1, 17) * C2;
    h ^= a0;
    h = rotate32(h, 19);
    h = h * 5 + C3;
    h ^= a2;
    h = rotate32(h, 19);
    h = h * 5 + C3;
    g ^= a1;
    g = rotate32(g, 19);
    g = g * 5 + C3;
    g ^= a3;
    g = rotate32(g, 19);
    g = g * 5 + C3;
    f += a4;
    f = rotate32(f, 19);
    f = f * 5 + C3;
    size_t iters = (len - 1) / 20;
    do {
        uint32_t a0 = rotate32(fetch32(s) * C1, 17) * C2;
        uint32_t a1 = fetch32(s + 4);
        uint32_t a2 = rotate32(fetch32(s + 8) * C1, 17) * C2;
        uint32_t a3 = rotate32(fetch32(s + 12) * C1, 17) * C2;
        uint32_t a4 = fetch32(s + 16);
        h ^= a0;
        h = rotate32(h, 18);
        h = h * 5 + C3;
        f += a1;
        f = rotate32(f, 19);
        f = f * C1;
        g += a2;
        g = rotate32(g, 18);
        g = g * 5 + C3;
        h ^= a3 + a1;
        h = rotate32(h, 19);
        h = h * 5 + C3;
        g ^= a4;
        g = bswap_32(g) * 5;
        h += a4 * 5;
        h = bswap_32(h);
        f += a0;
        tmp = f;
        f = g;
        g = h;
        h = tmp;
        s += 20;
    } while (--iters != 0);
    g = rotate32(g, 11) * C1;
    g = rotate32(g, 17) * C1;
    f = rotate32(f, 11) * C1;
    f = rotate32(f, 17) * C1;
    h = rotate32(h + g, 19);
    h = h * 5 + C3;
    h = rotate32(h, 17) * C1;
    h = rotate32(h + f, 19);
    h = h * 5 + C3;
    h = rotate32(h, 17) * C1;
    return h;
}
