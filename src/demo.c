#include <assert.h>
#include "widget.h"
#include "container.h"
#include "stuff.h"

// function called by filebrowser on success
void file_open_callback(struct string s, void* data)
{
    struct widget* w = data;
    widget_set_text(w, str_duplicate(s), true);
}

// a widget event handler. w is the widget that recieved the event. widgets recieve events
// if the mouse is over it or if the widget is "armed". rect contains the absolute position
// of the widget in the top level container. w->rect may have relative positions, rect has
// the true coordinates. you may put your onw data into w->payload
bool open_button_event(struct widget* w, const SDL_Event* event, const SDL_Rect* rect)
{
    // widget_changed() returns true if the widget state has chaned in a meaningfull way.
    // what exacly happened depends on the widget. in case of a button it meas it has been
    // ckicked. calling the function clears the changed state, alling it a second time will
    // return false.
    if (!widget_changed(w))
        return false;   // the event wasn't handled, maybe other are interested...
    // pass the label widget along to filebrowser callback
    filebrowser_show(STR("."), file_open_callback, w->payload, 0);
    return true;        // true causes the event to stop being be processed
}

int main(void)
{
    SDL_Init(SDL_INIT_VIDEO | SDL_INIT_AUDIO);
    TTF_Init();

    char* base_path = SDL_GetBasePath();
    struct string font_path = str_new_format("%s%s", base_path, "res/Comfortaa-Regular.ttf");
    widget_set_default_font(font_path, 14); // should be called at the beginning or bad things may happen

    struct widget* window = widget_new(WIDGET_WINDOW, SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, 800, 600, NULL);
    widget_set_text(window, STR("ke2k toolkit demo"), false);

    struct widget* label = widget_new(WIDGET_LABEL, 10, 10, 150, 25, window);
    widget_set_text(label, STR("Label"), false);

    struct widget* button = widget_new(WIDGET_BUTTON, 10, 40, 150, 25, window);
    widget_set_text(button, STR("Button"), false);

    struct widget* toggle = widget_new(WIDGET_TOGGLE, 10, 70, 150, 25, window);
    widget_set_text(toggle, STR("Toggle"), false);

    struct widget* control = widget_new(WIDGET_CONTROL, 10, 100, 150, 25, window);
    widget_set_range(control, -6, 24, true);
    widget_set_value(control, 12);

    struct widget* edit = widget_new(WIDGET_EDIT, 10, 130, 310, 25, window);
    widget_set_text(edit, str_new("Edit"), true);

    struct string list_data[] = {STRI("List item 0"), STRI("List item 1"), STRI("List item 2"),
                                 STRI("List item 3"), STRI("List item 4"), STRI("List item 5"),
                                 STRI("List item 6"), STRI("List item 7"), STRI("List item 8")};
    struct string_list strlist = {list_data, COUNTOF(list_data), 0};
    struct widget* list = widget_new(WIDGET_LIST, 170, 10, 150, 115, window);
    widget_set_list(list, strlist, false);

    // note that these widgets are added to panel, not window. positions are relative to parent
    struct widget* panel = widget_new(WIDGET_PANEL, 10, 160, 310, 60, window);
    struct widget* file_label = widget_new(WIDGET_LABEL, 5, 35, -5, 25, panel);
    struct widget* open_button = widget_new(WIDGET_BUTTON, -100, 5, -5, 25, panel);
    widget_set_text(open_button, STR("Open File"), false);
    widget_set_event(open_button, open_button_event, file_label);

    // widget position is relative to parent widget.
    // negative values set position right/bottom of parent.
    struct widget* maeplabel = widget_new(WIDGET_LABEL, -100, -30, 100, 25, window);
    widget_set_text(maeplabel, STR("m\303\246p \302\244 2013"), false);

    // main event loop. the widgets need to do their own window management.
    // those two functions should be called in that order. if you have many
    // redraws you may limit widget_paint_all calls.
    SDL_Event event = {0};
    while (event.type != SDL_QUIT) {
        SDL_WaitEvent(&event);
        widget_event_all(&event);   // dispatch event to all widgets
        widget_paint_all();         // lazy redraw of all windows; only areas that changed
    }

    // some frees so reduce valgrind noise
    widget_free_all(window);
    str_free(font_path);
    SDL_free(base_path);
    TTF_Quit();
    SDL_Quit();
    return 0;
}
