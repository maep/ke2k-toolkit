solution "ke2k"
    configurations {"Debug", "Release"}
    project "ke2k-demo"
        kind "WindowedApp"
        language "c"
        files "src/*.c"
        configuration "linux"
            buildoptions {"-std=c99", "-pedantic", "`pkg-config --cflags sdl2 SDL2_ttf`"}
            linkoptions {"-lm", "`pkg-config --libs sdl2 SDL2_ttf`"}
        configuration "Debug"
            flags {"ExtraWarnings", "Symbols"}
        configuration "Release"
            flags {"ExtraWarnings", "Optimize", "EnableSSE2", "FloatFast"}
